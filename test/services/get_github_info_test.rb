require 'test_helper'

class GetGithubInfoTest < ActiveSupport::TestCase

  setup do
    @user_data = GetGithubInfo.new("eser")
  end

  test "get userdata with correct github name" do
    assert_equal true, @user_data.perform.include?("Ruby")
  end

  test "group array of object regarding to language" do
    params = [
      {"language": "Ruby"},
      {"language": "Ruby"},
      {"language": "Python"}
    ].as_json

    result = @user_data.send(:calculate, params)
    assert_equal 2, result["Ruby"]
    assert_equal 1, result["Python"]
  end

  test 'should return which language has the most highest value' do
    params = {
      "Ruby": 2,
      "Python": 1
    }.as_json

    result = @user_data.send(:chose_max, params)
    assert_equal ["Ruby", 2], result
  end

end
