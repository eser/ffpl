# FFL - Find Favourite Programming Languages #

This application created with Rails 5.0.1 and deployed heroku with using docker.

You can reach the application using this link: https://sh24dexample.herokuapp.com/

## Deployment ##

If you wanted to deploy this application to heroku, wou would follow this easy steps:

Execute this commands in your command line:

1. heroku plugins:install heroku-container-registry
2. heroku container:login
3. git clone git@bitbucket.org:eser/ffpl.git
4. cd ffpl
5. heroku create
6. heroku container:push web
7. heroku open

That is it!

## Tests ##

You can execute tests for this application. These are the steps for you have to follow:

1. Go to application directory through terminal application
2. bundle install
3. rails db:create
4. rails db:migrate 
5. rake