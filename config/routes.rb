Rails.application.routes.draw do

  root to: 'main#index'

  post  'fetch_data' => 'main#fetch_data'

end
