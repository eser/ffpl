class MainController < ApplicationController

  def index
  end

  def fetch_data
    @userdata = GetGithubInfo.new(github_param["username"]).perform
  end

  private

  def github_param
    params.permit(:username)
  end

end
