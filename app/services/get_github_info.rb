class GetGithubInfo

  def initialize(username)
    @username = username
  end

  def perform
    user_data = get_data_from_api(@username)
    result = calculate(user_data.body)
    chose_max(result)
  # catching all exceptions and returning blank array for handling error message
  rescue => e
    []
  end

  private

  def chose_max(result_hash)
    result_hash.max_by{ |k,v| v }
  end

  # this method uses Ruby API client for fetching user data from Github
  def get_data_from_api(username)
    github_response = Github.repos.list("user" => username)
    # this is anotherway for finding popular language
    # GithubFavoriteLanguage.new(username: username).favorite_language
  end

  # this method calculates user's favorite languages
  def calculate(data)
    data.map{ |x| x["language"] }.compact.each_with_object(Hash.new(0)){ |i, h| h[i] += 1 }
  end

end
