FROM ruby:2.3
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs libsqlite3-dev && rm -rf /var/lib/apt/lists/*

RUN useradd -m deploy

COPY . /app

RUN chown deploy /app
RUN chmod -R 777 /app


USER deploy

ENV PORT=5000
ENV RACK_ENV=production
ENV RAILS_ENV=production
RUN export SECRET_KEY_BASE=$SECRET_KEY_BASE


WORKDIR /app
RUN gem install bundler
RUN bundle install --deployment --jobs 20 --retry 5
RUN bundle exec rails db:migrate

RUN bundle exec rake SECRET_KEY_BASE=$SECRET_KEY_BASE  assets:precompile



# Run the app.  CMD is required to run on Heroku
# $PORT is set by Heroku

CMD bundle exec puma -C config/puma.rb

